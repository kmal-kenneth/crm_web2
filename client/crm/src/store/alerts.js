export default {
  namespaced: true,
  state: () => ({
    alerts: []
  }),
  mutations: {
    addAlert(state, payload) {
      state.alerts.push(payload);
    },

    deleteAlert(state, payload) {
      let index = state.alerts.findIndex(
        alert => alert.message === payload.message
      );
      state.alerts.splice(index, 1);
    }
  },
  actions: {
    async addAlert(context, payload) {
      context.commit("addAlert", payload);
    },

    async deleteAlert(context, payload) {
      context.commit("deleteAlert", payload);
    }
  },
  getters: {}
};
