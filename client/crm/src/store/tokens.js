export default {
  namespaced: true,
  state: () => ({
    token: ""
  }),
  mutations: {
    setToken(state, token) {
      state.token = token;
    }
  },
  actions: {
    saveToken(context, token) {
      context.commit("setToken", token);
      localStorage.setItem("token", token);
    }
  },
  getters: {
    getToken(state) {
      let token;

      if (state.token != "") {
        token = state.token;
      } else {
        let storageToken = localStorage.getItem("token");

        if (storageToken) {
          token = storageToken;
        }
      }

      console.log("Token geted:", token);

      return token;
    }
  }
};
