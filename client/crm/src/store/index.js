import Vue from "vue";
import Vuex from "vuex";
import users from "./users";
import tokens from "./tokens";
import alerts from "./alerts";

Vue.use(Vuex);

export default new Vuex.Store({
  state: { baseUri: "http://localhost:3000/" },
  mutations: {},
  actions: {},
  modules: { users, tokens, alerts }
});
