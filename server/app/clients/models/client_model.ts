import { model, Schema } from "mongoose";
const sector = require("../../sectors/models/sector_model");

const tmpSchema = new Schema({
  name: String,
  legalIdentityCard: String,
  webPage: String,
  physicalAddress: String,
  phoneNumber: String,
  sector: sector.schema,
});

const tmpModel = model("client", tmpSchema);

module.exports = { schema: tmpSchema, model: tmpModel };
