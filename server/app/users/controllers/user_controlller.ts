import { Request, Response } from "express";
const config = require("../../configs/config");
const url = `${config.url}:${config.port}`;

const { model: User } = require("../models/user_model");
type IUser = typeof User;
/**
 * Creates a user
 *
 * @param {*} req
 * @param {*} res
 */
const userPost = (req: Request, res: Response) => {
  var user = new User({
    name: req.body.name,
    lastName: req.body.lastName,
    userName: req.body.userName,
    password: req.body.password,
    admin: req.body.admin,
  });

  if (user.name && user.lastName && user.userName && user.password) {
    user.save(function (err: any) {
      if (err) {
        console.log("Error while saving the user", err);
        res.status(422);
        res.json({
          message: "There was an error saving the user",
        });
      }
      res.status(201); //CREATED
      res.header({
        location: `${url}/api/users/?id=${user.id}`,
      });
      res.json(user);
    });
  } else {
    console.log(
      "Error while saving the user: require name, lastname, username, password"
    );
    res.status(422);
    res.json({
      message: "No valid data provided for user",
    });
  }
};

/**
 * Get all users
 *
 * @param {*} req
 * @param {*} res
 */
const userGet = (req: Request, res: Response) => {
  // if an specific user is required
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err: any, user: IUser) {
      if (err) {
        res.status(404);
        console.log("message while queryting the user", err);
        res.json({ message: "user doesnt exist" });
      }
      res.json(user);
    });
  } else {
    // get all users
    User.find(function (err: any, users: IUser) {
      if (err) {
        res.status(422);
        res.json({ message: err });
      }
      res.json(users);
    });
  }
};

/**
 * Updates a user
 *
 * @param {*} req
 * @param {*} res
 */
const userPatch = (req: Request, res: Response) => {
  // get user by id
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err: any, user: IUser) {
      if (err) {
        res.status(404);
        console.log("message while queryting the user", err);
        res.json({ message: "user doesnt exist" });
      }

      // update the user object (patch)

      user.name = req.body.name ? req.body.name : user.name;
      user.lastName = req.body.lastName ? req.body.lastName : user.lastName;
      user.userName = req.body.userName ? req.body.userName : user.userName;
      user.password = req.body.password ? req.body.password : user.password;
      user.admin = req.body.admin;

      user.save(function (err: any) {
        if (err) {
          res.status(422);
          console.log("message while saving the user", err);
          res.json({
            message: "There was an message saving the user",
          });
        }
        res.status(200); // OK
        res.json(user);
      });
    });
  } else {
    res.status(404);
    res.json({ message: "user doesnt exist" });
  }
};

/**
 * Delete a user
 *
 * @param {*} req
 * @param {*} res
 */
const userDelete = (req: Request, res: Response) => {
  // get user by id
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err: any, user: IUser) {
      if (err) {
        res.status(404);
        console.log("message while queryting the user", err);
        res.json({ message: "user doesnt exist" });
      }

      user.remove(
        {
          _id: req.params.id,
        },
        function (err: any, user: IUser) {
          if (err) {
            res.status(422);
            console.log("message while deleting the user", err);
            res.json({
              message: "There was an message deleting the user",
            });
          }
          res.status(204); // OK
          res.json({
            message: "user deleted",
          });
        }
      );
    });
  } else {
    res.status(422);
    res.json({ message: "Requires user id." });
  }
};

module.exports = {
  userPost,
  userGet,
  userPatch,
  userDelete,
};
