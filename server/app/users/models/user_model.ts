import { model, Schema, Model, Document } from "mongoose";

const tmpSchema = new Schema({
  name: String,
  lastName: String,
  userName: String,
  password: String,
  admin: Boolean,
});

interface IUser extends Document {
  name: String;
  lastName: String;
  userName: String;
  password: String;
  admin: Boolean;
}

const tmpModel: Model<IUser> = model<IUser>("user", tmpSchema);

module.exports = { schema: tmpSchema, model: tmpModel };
