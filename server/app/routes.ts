import { Request, Response, Application } from "express";
import express = require("express");
import jwt from "jsonwebtoken";

module.exports = function (app: Application) {
  /**
   * Login with jwt
   * @params
   * req Request
   * res Response
   */
  app.post("/login", function (req: Request, res: Response) {
    if (req.body.username === "user" && req.body.password === "pass") {
      const payload = {
        check: true,
      };

      const token = jwt.sign(payload, app.get("secretKey"), {
        expiresIn: 60 * 60 * 24,
      });

      res.json({
        message: "Login correcto",
        token: token,
      });
    } else {
      res.status(401).send({
        message: "Username or password incorrect",
      });
    }
  });

  // Conig jwt protect routes
  const protectRoutes = express.Router();

  require("./middlewares")(app, protectRoutes);

  const apiUrl = "/api/";

  //user routes
  const {
    userPost,
    userGet,
    userPatch,
    userDelete,
  } = require("./users/controllers/user_controlller");

  app.post(`${apiUrl}users`, protectRoutes, userPost);
  app.get(`${apiUrl}users`, protectRoutes, userGet);
  app.patch(`${apiUrl}users`, protectRoutes, userPatch);
  app.put(`${apiUrl}users`, protectRoutes, userPatch);
  app.delete(`${apiUrl}users`, protectRoutes, userDelete);
};
