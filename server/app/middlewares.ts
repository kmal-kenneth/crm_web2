import { Application, Router, NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";

module.exports = function (app: Application, protectRoutes: Router) {
  // Middleware for protect routes
  protectRoutes.use(function (req: Request, res: Response, next: NextFunction) {
    if (req.headers["authorization"]) {
      const token = req.headers["authorization"].split(" ")[1];
      console.log("token: ", token);

      if (token) {
        jwt.verify(token, app.get("secretKey"), function (
          err: any,
          decoded: any
        ) {
          if (err) {
            res.status(400).send({
              message: "Invalid token",
            });
          } else {
            req.body.decoded = decoded;
            next();
          }
        });
      } else {
        res.status(401).send({
          message: "Token not provided",
        });
      }
    } else {
      res.status(400).send({
        message: "Authorization not provided",
      });
    }
  });
};
