// database connection
const mongoose = require("mongoose");
const configuration = require("./configs/config");

mongoose.connect(
  `${configuration.dbUrl}`,
  { useNewUrlParser: true, useUnifiedTopology: true },
  (err: any) => {
    if (err) console.error("mongoose error:", err);
    else console.log("Connected to the mongodb");
  }
);

var db = mongoose.connection;
