import { model, Schema } from "mongoose";

const tmpSchema = new Schema({
  name: String,
});

const tmpModel = model("state", tmpSchema);

module.exports = { schema: tmpSchema, model: tmpModel };
