import { model, Schema } from "mongoose";
const client = require("../../clients/models/client_model");
const state = require("../../support_tickets/models/support_ticket_model");

const tmpSchema = new Schema({
  problemTitle: String,
  problemDetails: String,
  informer: String,
  client: client.schema,
  state: state.schema,
});

const tmpModel = model("support_ticket", tmpSchema);

module.exports = { schema: tmpSchema, model: tmpModel };
