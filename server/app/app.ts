const express = require("express"),
  bodyParser = require("body-parser"),
  cors = require("cors"),
  config = require("./configs/config");

require("./db");

// Create a new express aplication
const app = express();

app.use(
  cors({
    domains: "*",
    methods: "*",
  })
);
app.set("secretKey", config.secretKey);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

require("./routes")(app);

// Init server
app.listen(config.port, function () {
  console.log(`Example app listening on port ${config.port}!`);
});
