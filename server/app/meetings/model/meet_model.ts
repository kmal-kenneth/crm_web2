import { model, Schema } from "mongoose";
const user = require("../../users/models/user_model");
const client = require("../../clients/models/client_model");

const tmpSchema = new Schema({
  meetingTitle: String,
  dateTime: Date,
  assignedUser: [user.schema],
  isVirtual: Boolean,
  client: client.schema,
});

const tmpModel = model("sector", tmpSchema);

module.exports = { schema: tmpSchema, model: tmpModel };
