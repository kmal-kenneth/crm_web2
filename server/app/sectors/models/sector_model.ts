import { model, Schema } from "mongoose";

const tmpSchema = new Schema({
  name: String,
});

const tmpModel = model("sector", tmpSchema);

module.exports = { schema: tmpSchema, model: tmpModel };
