module.exports = {
  secretKey: "MySecretPass",
  url: "http://localhost",
  port: "3000",
  dbUrl: "mongodb://127.0.0.1:27017/todo-api",
};
