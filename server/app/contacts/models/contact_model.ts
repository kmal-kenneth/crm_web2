import { model, Schema } from "mongoose";
const client = require("../../clients/models/client_model");

const tmpSchema = new Schema({
  client: client.schema,
  name: String,
  lastName: String,
  email: String,
  phoneNumber: String,
  jobPosition: String,
});

const tmpModel = model("sector", tmpSchema);

module.exports = { schema: tmpSchema, model: tmpModel };
